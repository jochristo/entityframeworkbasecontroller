﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkBaseController.Exceptions
{
   [Serializable()]
   public class EntityHasNavigationDependencyException : Exception
   {
      public EntityHasNavigationDependencyException() : base() {}
      public EntityHasNavigationDependencyException(string message) : base(message) {}
      public EntityHasNavigationDependencyException(string message, Exception inner) : base(message, inner) { }
   }
}
