﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkBaseController.Exceptions
{
   [Serializable()]
   public class NavigationPropertyNotFoundException : Exception
   {
      public NavigationPropertyNotFoundException() : base() {}
      public NavigationPropertyNotFoundException(string message) : base(message) {}
      public NavigationPropertyNotFoundException(string message, Exception inner) : base(message, inner) { }
   }
}
