﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkBaseController.Exceptions
{
   [Serializable()]
   public class EntityExistsException : Exception
   {
      public EntityExistsException() : base() {}
      public EntityExistsException(string message) : base(message) {}
      public EntityExistsException(string message, Exception inner) : base(message, inner) { }
   }
}
