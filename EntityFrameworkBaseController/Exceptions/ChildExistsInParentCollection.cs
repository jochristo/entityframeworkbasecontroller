﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseContext.Exceptions
{
   [Serializable()]
   public class ChildExistsInParentCollection : Exception
   {
      public ChildExistsInParentCollection() : base(){}
      public ChildExistsInParentCollection(string message) : base(message) {}
      public ChildExistsInParentCollection(string message, Exception inner) : base(message, inner) { }
   }
}
