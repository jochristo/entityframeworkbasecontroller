﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkBaseController.Exceptions
{
   [Serializable()]
   class NavigationPropertyNameNotFoundException : Exception
   {
      public NavigationPropertyNameNotFoundException() : base() {}
      public NavigationPropertyNameNotFoundException(string message) : base(message) {}
      public NavigationPropertyNameNotFoundException(string message, Exception inner) : base(message, inner) { }
   }
}
