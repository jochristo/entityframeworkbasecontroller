﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkBaseController.Exceptions
{
   [Serializable()]
   public class NavigationPropertyExistsException : Exception
   {
      public NavigationPropertyExistsException() : base() {}
      public NavigationPropertyExistsException(string message) : base(message) {}
      public NavigationPropertyExistsException(string message, Exception inner) : base(message, inner) { }
   }
}
